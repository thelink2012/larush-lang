--[[
--]]

-- Output directory option
newoption {
    trigger = "location",
    value   = "path",
    description = "Output directory for the build files"
}

if not _OPTIONS["location"] then
    _OPTIONS["location"] = "build"
end


solution "larush-lang"

    configurations { "Debug", "Release" }

    targetdir "bin"
    implibdir "bin"
    location( _MAIN_SCRIPT_DIR .. '/' .. _OPTIONS["location"] )
    flags { "NoPCH" }

    includedirs {
        "deps/libantlr3c-3.4/include",
        "deps/tclap-1.2.1/include"
    }

    flags { "StaticRuntime" }

    configuration "Debug*"
        flags { "Symbols" }

    configuration "Release*"
        defines { "NDEBUG" }
        optimize "Speed"

    configuration "windows"
        includedirs { "deps/dirent-1.20.1/include" }

    project "larush-lang"
        configuration {}
        language "C++"
        kind "ConsoleApp"

        files { "src/**.c", "src/**.cpp", "src/**.h", "src/**.hpp" }
        files { "src/**.g" }
        links { "libantlr" }
        defines { "_empty=NULL" }    -- ANTLR bug
        includedirs { "src/RushLangParser" }

        --[[configuration "windows"
            prebuildcommands { "cd src & RushLangSource.bat & cd .." }
        configuration "not windows"
            prebuildcommands { "cd src ; RushLangSource.sh  ; cd .." }
        --]]


    project "libantlr"
        language "C"
        kind "StaticLib"
        files { "deps/libantlr3c-3.4/src/**.c", "deps/libantlr3c-3.4/include/**.h" }

