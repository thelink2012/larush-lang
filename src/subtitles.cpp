#include <iostream>
#include <fstream>
#include <algorithm>
#include <dirent.h>
#include <memory>
#include <cstring>
#include "rush.hpp"

struct DIR_DELETER
{
    void operator()(DIR* p)
    {
        closedir(p);
    }
};

using unique_dir = std::unique_ptr<DIR, DIR_DELETER>;


RushSubtitleManager& RushSubtitleManager::ReadFromBinary(const std::string& dirname)
{
    this->Clear();

    try
    {
        unique_dir dir(opendir(dirname.c_str()));
        if(!dir) throw RushException(std::string("Failed to open directory '") + dirname + "' for reading");

        while(auto entry = readdir(dir.get()))
        {
            if(!S_ISDIR(entry->d_type))
            {
                if(!strcasecmp(GetExtension(entry->d_name).data(), "dat"))
                    this->ReadFromBinary(dirname + '/' + entry->d_name, -1);
            }
        }
    }
    catch(...)
    {
        this->Clear();
        throw;
    }

    return *this;
}

RushSubtitleManager& RushSubtitleManager::ReadFromBinary(const std::string& filepath, int)
{
    uint32_t magic, numsubs, numlangs;
    uint32_t relative = 4 + 4;

    auto filename = GetLastComponent(filepath);

    std::cout << "Reading binary language file '" << filename << "'." << std::endl;

    std::streamoff begin_text = (std::numeric_limits<std::streamoff>::max)();
    std::ifstream stream(filepath, std::ios::binary);
    std::ifstream estream(filepath + ".EXTRA", std::ios::binary);
    stream.clear(); estream.clear();
    stream.exceptions(std::ios::failbit | std::ios::badbit);
    estream.exceptions(std::ios::failbit | std::ios::badbit);

    if(stream.is_open() == false)
        throw RushException(std::string("Failed to open binary stream for reading '") + filename + "'.");

    std::vector<uint32_t> aTime;
    std::vector<std::vector<uint32_t>> aaOffsets;

    auto SetBeginText = [&begin_text](uint32_t offset)
    {
        if(offset < begin_text) begin_text = offset;
    };

    auto ReadOffsets = [&]
    {
        auto& aOffsets = *aaOffsets.emplace(aaOffsets.end());
        aOffsets.resize(numsubs);
        stream.read((char*)aOffsets.data(), sizeof(aOffsets[0]) * numsubs);
        std::for_each(aOffsets.begin(), aOffsets.end(), SetBeginText);
    };

    auto IsAtTextBlock = [&]
    {
        return std::streamoff(stream.tellg()) >= begin_text + relative;
    };
    
    auto SkipSeparator = [&stream]
    {
        uint32_t z;
        stream.read((char*)&z, sizeof(z));
        if(z != 0x0) throw RushException("Subtitle language offset separator is not 0x0");
    };

    // Read the header
    //stream.read((char*)&magic, 4);
    stream.read((char*)&magic, sizeof(magic));
    stream.read((char*)&numsubs, sizeof(numsubs));
    if(magic != 0x00000002)
    {
        std::cout << "File '" + filepath << "' does not have correct magic, skipping it." << std::endl;
        return *this;
    }
    if(!numsubs) return *this;

    aTime.resize(numsubs);
    stream.read((char*)aTime.data(), sizeof(aTime[0]) * numsubs);
    relative += aTime.size() * 4;

    ReadOffsets();

    if(IsAtTextBlock() == false)
    {
        SkipSeparator();
        while(!IsAtTextBlock())
        {
            ReadOffsets();
            SkipSeparator();
        }
    }

    numlangs = aaOffsets.size();

    langs.resize(numlangs);
    for(auto l = 0u; l < numlangs; ++l)
    {
        auto& lang = langs[l];

        auto labelname = RemoveExtension(filename);
        if(std::any_of(lang.begin(), lang.end(), PredFindRushMap(labelname)))
            throw RushException(" TODO ALREADY EXIST LABEL ");

        //auto xpair = lang.emplace(std::piecewise_construct, std::make_tuple(labelname), std::make_tuple());
        //if(!xpair.second) throw RushException(" TODO ALREADY EXIST LABEL ");
        //auto& labels = xpair.first->second;
        lang.emplace_back(std::piecewise_construct, std::make_tuple(labelname), std::make_tuple());
        auto& labels = lang.back().second;

        for(auto i = 0u; i < numsubs; ++i)
        {
            labels.emplace_back(ReadString(stream, (estream.is_open()? &estream : nullptr), aaOffsets[l][i] + relative), aTime[i]);
        }
    }

    return *this;
}


RushSubtitleManager& RushSubtitleManager::WriteToSource(const std::vector<std::string>& files)
{
    for(auto i = 0u; i < files.size() && i < langs.size(); ++i)
    {
        std::ofstream stream(files[i], std::ios::trunc);
        stream.exceptions(std::ios::failbit | std::ios::badbit);

        for(auto& label : langs[i])
        {
            stream << '[' << label.first << "]\n";
            for(auto& str : label.second)
                stream << '"' << EscapeString(str.first) << "\"@" << str.second << '\n';
            stream << '\n';
        }
    }
    return *this;
}


RushSubtitleManager& RushSubtitleManager::ReadFromSource(const std::vector<std::string>& files)
{
    for(auto& file : files) this->ReadFromSource(file);
    return *this;
}

RushSubtitleManager& RushSubtitleManager::ReadFromSource(const std::string& filename)
{
    RushParser parser(filename.c_str());
    this->langs.emplace_back(std::move(parser.GetLabels()));
    return *this;
}

RushSubtitleManager& RushSubtitleManager::WriteToBinary(const std::string& dirname)
{
    struct {
        uint32_t magic;
        uint32_t numsubs;
    } header;


    using lvec = std::vector<std::reference_wrapper<RushLabel>>;
    std::map<std::string, lvec> labels;

    for(auto& lang : this->langs)
    {
        for(auto& lb : lang)
        {
            labels[lb.first].emplace_back(std::ref(lb.second));
            RushLabel& entries1 = labels[lb.first].front();
            RushLabel& entries2 = lb.second;

            if(entries1.size() != entries2.size())
                throw RushException("Label '" + lb.first + "' does not have the same amount of entries in all sources.");;

            for(auto i = 0u; i < entries1.size(); ++i)
            {
                if(entries1[i].second != entries2[i].second)
                    throw RushException("Label '" + lb.first + "' contains an entry that does not have the same timing in all sources.");
            }
        }
    }

    if(labels.empty()) return *this;


    for(auto& label : labels)
    {
        auto filename = dirname + '/' + label.first + ".dat";
        std::ofstream stream(filename, std::ios::trunc | std::ios::binary);
        std::ofstream estream(filename + ".EXTRA", std::ios::trunc | std::ios::binary);

        if(!stream.is_open() || !estream.is_open())
            throw RushException(std::string("Failed to open binary files for writing '") + filename + "'.");

        stream.exceptions(std::ios::failbit | std::ios::badbit);
        estream.exceptions(std::ios::failbit | std::ios::badbit);

        uint32_t relative, strblock, zero32 = 0;
        auto& langs = label.second;
        auto& offs = FindOffsetForStrings(langs);
        auto& times = BuildTimeArrayForLabel(langs);

        auto make_delims = langs.size() > 1;

        header.magic   = 0x00000002;
        header.numsubs = langs.size()? langs.front().get().size() : 0;

        stream.write((char*)(&header), sizeof(header));
        stream.write((char*)(times.data()), sizeof(times[0]) * times.size());
        relative = (uint32_t) stream.tellp();

        for(auto i = 0u; i < offs.size(); ++i)
        {
            auto off = offs[i].second + (offs.size() * sizeof(uint32_t)) + ((!make_delims? 0 : langs.size()) * sizeof(uint32_t));
            stream.write((char*)&off, sizeof(off));

            if(make_delims && ((i % header.numsubs) == header.numsubs - 1))
                stream.write((char*)&zero32, sizeof(zero32));
        }

        strblock = (uint32_t) stream.tellp();
        for(auto& off : offs)
        {
            stream.seekp(strblock + off.second);
            stream.write(off.first.get().data(), off.first.get().length());
            stream.put(0);
        }

        stream.seekp(std::ios::off_type(MAX_LANGUAGES - langs.size()) - 1, std::ios::end);
        estream.seekp(stream.tellp());
        stream.put(0); estream.put(0);
    }
    
    return *this;
}
