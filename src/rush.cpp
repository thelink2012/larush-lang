#include "rush.hpp"

/*
 *  RushLangManager::ReadString
 *      Reads a string at the specified 'offset' in the binary file 'stream' (type of text encoding specified in the file 'encoding')
 */
std::string RushLangManager::ReadString(std::istream& stream, std::istream* encoding, std::ios::streamoff offset)
{
    stream.seekg(offset);
    return ReadString(stream, encoding);
}

/*
 *  RushLangManager::ReadString
 *      Reads the string at the current position in the binary file 'stream' (type of text encoding specified in the file 'encoding')
 */
std::string RushLangManager::ReadString(std::istream& stream, std::istream* encoding)
{
    std::string buf;
    buf.reserve(32);

    // Seek encoding to the same position as the text stream
    if(encoding) encoding->seekg(stream.tellg());

    // Read the character until a null terminator
    while(char c = stream.get())
    {
        // We only support a ASCII (extended) 8bit encoding
        if(encoding && encoding->get() != 0) c = '\?';
        buf.push_back(c);
    }

    return buf;
}

/*
 *  RushLangManager::RemoveExtension
 *      Removes an extension from the filename
 */
std::string RushLangManager::RemoveExtension(std::string filename)
{
    filename.erase(filename.find_last_of('.'));
    return filename;
}

/*
 *  RushLangManager::GetExtension
 *      Gets the extension from a filename
 */
std::string RushLangManager::GetExtension(std::string filename)
{
    auto pos = filename.find_last_of('.');
    if(pos != filename.npos) filename.erase(0, pos + 1);
    return filename;
}

/*
 *  RushLangManager::GetExtension
 *      Gets the last path component of a filepath
 */
std::string RushLangManager::GetLastComponent(std::string filename)
{
    auto pos = filename.find_last_of("/\\");
    if(pos != filename.npos) filename.erase(0, pos + 1);
    return filename;
}

/*
 *  RushLangManager::FindOffsetForStrings
 *      Calculates all offsets (0-based) for the strings at @label when they're stored continously
 */
auto RushLangManager::FindOffsetForStrings(const std::vector<std::reference_wrapper<RushLabel>>& labels) -> OffAssocList
{
    uint32_t offset = 0;
    OffAssocList offs;
    
    for(auto& label : labels)
    {
        for(auto& entry : label.get())
        {
            offs.emplace_back(std::ref(entry.first), offset);
            offset += entry.first.length() + 1;
        }
    }

    return offs;
}

/*
 *  RushLangManager::BuildTimeArrayForLabel
 *      Gets the timing for the subtitles to a array
 */
std::vector<uint32_t> RushLangManager::BuildTimeArrayForLabel(const std::vector<std::reference_wrapper<RushLabel>>& labels)
{
    std::vector<uint32_t> array;

    if(labels.size())
    {
        auto& entries = labels.front().get();   // Take the first language set of texts, the timing should be the same on all langs
        array.reserve(entries.size());
        for(auto& entry : entries)
            array.emplace_back(entry.second);
    }

    return array;
}

/*
 *  RushLangManager::EscapeString
 *      Escapes the string, that's translate the binary '\n' into a textual representation "\n"
 */
std::string RushLangManager::EscapeString(std::string string)
{
    std::string result;
    result.reserve(string.length());

    for(char& c : string)
    {
        switch(c)
        {
            case '"':  result.append("\\\""); break;
            case '\\': result.append("\\\\"); break;
            case '\r': result.append("\\r"); break;
            case '\t': result.append("\\t"); break;
            case '\v': result.append("\\v"); break;
            case '\b': result.append("\\b"); break;
            case '\f': result.append("\\f"); break;
            
            default:
                if(isprint((unsigned char)c) == false)
                    result.push_back(c);
                else
                    result.push_back(c);
                break;
        }
    }

    return result;
}
