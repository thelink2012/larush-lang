#pragma once
#pragma warning(disable : 4503) // decorated name length exceeded, name was truncated
#include <map>
#include <vector>
#include <istream>
#include <cstdint>
#include <cstring>
#include <string>
#include <stdexcept>


#ifdef _WIN32
#define strcasecmp  _stricmp
#define strncasecmp _strnicmp
#endif

struct ANTLR3_BASE_TREE_struct;
using RushSub   = std::pair<std::string, uint32_t>;
using RushLabel = std::vector<RushSub>;

//using RushMap = std::map<std::string, RushLabel>;
using RushMap = std::vector<std::pair<std::string, RushLabel>>;

static const int MAX_LANGUAGES = 5; // TODO DO CHECKS

struct PredFindRushMap
{
    std::string find;

    PredFindRushMap(std::string find) : find(std::move(find))
    {}

    bool operator()(const RushMap::value_type& kv)
    {
        return kv.first == find;
    }
};

struct RushParser
{
    RushParser()                     {}
    RushParser(const char* filename) { ReadRushSource(filename); }

    void ReadRushSource(const char* filename);

    RushMap& GetLabels() { return labels; }

    private:

    RushMap labels;
    RushLabel* currentLabel;

    
    void ReadRushTree(ANTLR3_BASE_TREE_struct*);
    void ReadRushLabel(ANTLR3_BASE_TREE_struct*);

    template<class FuncT, class ...Args>
    void ParseRushTree(ANTLR3_BASE_TREE_struct*, const std::pair<int, FuncT>&, Args&&...);
    void ParseRushTree(ANTLR3_BASE_TREE_struct*) {}

    bool OnLabelRule(ANTLR3_BASE_TREE_struct*);
    bool OnSubtitleRule(ANTLR3_BASE_TREE_struct*);
};


using RushException = std::runtime_error;



struct RushLangManager
{
    static std::string RemoveExtension(std::string);
    static std::string GetLastComponent(std::string);
    static std::string GetExtension(std::string);

    using OffAssocList = std::vector<std::pair<std::reference_wrapper<std::string>, uint32_t>>;

    static std::string ReadString(std::istream& stream, std::istream* encoding, std::ios::streamoff offset);
    static std::string ReadString(std::istream& stream, std::istream* encoding);

    static OffAssocList FindOffsetForStrings(const std::vector<std::reference_wrapper<RushLabel>>&);
    static std::vector<uint32_t> BuildTimeArrayForLabel(const std::vector<std::reference_wrapper<RushLabel>>&);


    static std::string EscapeString(std::string);


};

struct RushSubtitleManager : public RushLangManager
{
    std::vector<RushMap> langs;

    void Clear() { langs.clear(); }

    RushSubtitleManager& ReadFromBinary(const std::string& dirname);
    RushSubtitleManager& ReadFromBinary(const std::string& filename, int dummy);
    RushSubtitleManager& WriteToSource(const std::vector<std::string>& langfiles);

    RushSubtitleManager& ReadFromSource(const std::vector<std::string>& langfiles);
    RushSubtitleManager& ReadFromSource(const std::string& filename);
    RushSubtitleManager& WriteToBinary(const std::string& dirname);
    
};

struct RushLocalFrontendManager : public RushLangManager
{
    RushLabel label;

    RushLocalFrontendManager& ReadFromBinary(const std::string& filename);
    RushLocalFrontendManager& ReadFromSource(const std::string& filename);
    RushLocalFrontendManager& WriteToBinary(const std::string& filename);
    RushLocalFrontendManager& WriteToSource(const std::string& filename);
};

struct RushAppTextManager : public RushLangManager
{
    RushLabel label;

    RushAppTextManager& ReadFromBinary(const std::string& filename);
    RushAppTextManager& ReadFromSource(const std::string& filename);
    RushAppTextManager& WriteToBinary(const std::string& filename);
    RushAppTextManager& WriteToSource(const std::string& filename);
};

