@echo off
setlocal EnableDelayedExpansion
setlocal EnableExtensions

rem http://batsh.org/
set CLASSPATH=!CLASSPATH!^;../deps/antlr-3.5.2.jar
java org.antlr.Tool RushLangSource.g -o RushLangParser