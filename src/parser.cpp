#include "rush.hpp"
#include <memory>
#include <functional>
#include <algorithm>
using namespace std::placeholders;

#define _ALLOW_KEYWORD_MACROS
#include <RushLangSourceLexer.h>
#include <RushLangSourceParser.h>
#undef true         // Damn ANTLR3
#undef false        //

// RushException with AST node description
class RushParseError : public RushException
{
    public:
        RushParseError(pANTLR3_BASE_TREE node, const char* what)
        : RushException(std::to_string(node->getLine(node)) + ':' + std::to_string(node->getCharPositionInLine(node)) + ": " + what) 
        {}
};


// Deleter for ANTLR3 objects (for std::unique_ptr)
template<typename T>
struct ANTLR3_DELETER
{
    void operator()(T* ptr) const
    {
        ptr->free(ptr);
    }
};

// Alias for unique_ptr for ANTLR3 objects (and so auto-deleting them on out-of-scope)
template<class T>
using unique_antlr = std::unique_ptr<T, ANTLR3_DELETER<T>>;


/*
 *  ProcessString
 *      Processes a ANTLR3 string, which contains the a unformatted string (as in >"\n\rWithout any transformation, literally including those quotes"<)
 *      and outputs a proper formatted to the binary level string.
 */
static std::string ProcessString(pANTLR3_STRING strnode)
{
    std::string result;
    result.reserve(strnode->len);
    
    // Iterate on the string until the ending quote (which should be followed by a null terminator)
    // Begin from the 2nd character since the first one is an '"'
    for(auto p = (const char*)(strnode->chars)+1; *p != '"'; ++p)
    {
        if(*p == '\\')  // Formating needed ahead?
        {
            switch(char c = *++p)
            {
                case 'n': result.push_back('\n'); break;
                case 'r': result.push_back('\r'); break;
                case 't': result.push_back('\t'); break; 
                case 'v': result.push_back('\v'); break;
                case 'b': result.push_back('\b'); break;
                case 'f': result.push_back('\f'); break;
                case '"': result.push_back('"'); break;
                case '\'': result.push_back('\''); break;
                case '\\': result.push_back('\\'); break;
                default: throw RushException("Invalid escape sequence."); break; // lol stupid
            }
        }
        else
            result.push_back(*p);
    }

    return result;
}


/*
 *  RushParser::ReadRushSource
 *      Parses a rush language file and captures all labels and texts.
 *      To acquire the result of the operation, call GetLabels()
 */
void RushParser::ReadRushSource(const char* filename)
{
    unique_antlr<ANTLR3_INPUT_STREAM> istream(antlr3FileStreamNew((pANTLR3_UINT8)filename, ANTLR3_ENC_8BIT));   // ASCII 'cuz the nature of rush lang files
    if(istream)
    {
        unique_antlr<RushLangSourceLexer> lexer(RushLangSourceLexerNew(istream.get()));
        if(lexer)
        {
            unique_antlr<ANTLR3_COMMON_TOKEN_STREAM> tstream(antlr3CommonTokenStreamSourceNew(ANTLR3_SIZE_HINT, TOKENSOURCE(lexer.get())));
            if(tstream)
            {
                unique_antlr<RushLangSourceParser> parser(RushLangSourceParserNew(tstream.get()));
                if(parser)
                {
                    auto syntaxTree = parser->content(parser.get());

                    if(parser->pParser->rec->state->errorCount == 0)
                    {
                        return ReadRushTree(syntaxTree.tree);
                    }
                    else
                    {
                        throw RushException(" TODO ERROR PARSER .");
                        //parser->pParser->rec->displayRecognitionError
                    }
                }
            }
        }
    }
    else throw RushException(std::string("Failed to open source file for reading '") + filename + "'.");
    throw RushException(std::string("Failed to parse source file '") + filename + "'.");
}


/*
 *  RushParser::ReadRushTree
 *      Analyzes the AST resulting from parsing the source file and build the labels array for output.
 */
void RushParser::ReadRushTree(pANTLR3_BASE_TREE tree)
{
    this->ParseRushTree(tree, std::make_pair(Identifier, std::bind(&RushParser::OnLabelRule, this, _1)), std::make_pair(SUBTITLE, std::bind(&RushParser::OnSubtitleRule, this, _1)));
}

/*
 *  RushParser::ParseRushTree
 *      Helper for analyzing the AST resulting from parsing the source file and build the labels array for output.
 *      When the AST node with type arg.first is found, it calls the function arg.second, goes to the child node and goes for the next argument is args.
 *      Notice arg.second receives the node being analyzed as argument and should return a bool indicating whether it should go to the child node or not.
 *      Annnnd arg.second can also be null (making it go to the child anyway).
 */
template<class FuncT, class ...Args>
void RushParser::ParseRushTree(pANTLR3_BASE_TREE tree, const std::pair<int, FuncT>& arg, Args&&... args)
{
    std::function<bool(pANTLR3_BASE_TREE)> func = arg.second;

    for(int i = 0, max = tree->getChildCount(tree); i < max; ++i)
    {
         auto node = (pANTLR3_BASE_TREE) tree->getChild(tree, i);
         auto x = node->getType(node);
         if(node->getType(node) == arg.first)
         {
             if(!func || func(node))
             {
                 this->ParseRushTree(node, std::forward<Args>(args)...);
             }
         }
    }
}


/*
 *  RushParser::OnLabelRule
 *      Called whenever a label ([LABEL]) is found on the AST analyze.
 */
bool RushParser::OnLabelRule(pANTLR3_BASE_TREE node)
{
    // Check out the label name
    auto labelname = (const char*) node->getText(node)->chars;
    if(*labelname == '\0') throw RushParseError(node, "Missing label name.");

    // Checks if the label has been already used
    if(std::any_of(labels.begin(), labels.end(), PredFindRushMap(labelname)))
        throw RushParseError(node, "Label repeated twice.");

    // Add to the array and set the current working label
    this->labels.emplace_back(std::piecewise_construct, std::make_tuple(labelname), std::make_tuple());
    this->currentLabel = &labels.back().second;

    // Continue the analyze on this node childs
    return true;
}

/*
 *  RushParser::OnLabelRule
 *      Called whenever a subtitle ("Subtitle"@time) is found on the AST analyze.
 */
bool RushParser::OnSubtitleRule(pANTLR3_BASE_TREE node)
{
    try
    {
        uint32_t time       = 0;                                            // Default time (if no '@time') is found on the sub)
        auto ntext          = ((pANTLR3_BASE_TREE)node->getChild(node, 0)); // 
        auto text           = ProcessString(ntext->getText(ntext));         // The actual subtitle text

        // If this subtitle node has two childs, the 2nd one is the '@time' property
        if(node->getChildCount(node) >= 2)
        {
            auto ntime = ((pANTLR3_BASE_TREE)node->getChild(node, 1));
            time = std::stoul((const char*)(ntime->getText(ntime)->chars));
        }

        // Push to the working labels object
        currentLabel->emplace_back(std::move(text), time);

        // Stop the analyze on this node, don't go further on childs
        return false;
    }
    catch(const RushException& e)   // For failures with ProcessString
    {
        throw RushParseError(node, e.what());
    }
}
