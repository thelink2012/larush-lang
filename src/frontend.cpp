#include <iostream>
#include <fstream>
#include "rush.hpp"


RushLocalFrontendManager& RushLocalFrontendManager::ReadFromBinary(const std::string& filename)
{
    uint32_t magic;

    std::ifstream stream(filename, std::ios::binary);
    std::ifstream estream(filename + ".EXTRA", std::ios::binary);
    stream.clear(); estream.clear();
    stream.exceptions(std::ios::failbit | std::ios::badbit);
    estream.exceptions(std::ios::failbit | std::ios::badbit);

    if(stream.is_open() == false)
        throw RushException(std::string("Failed to open binary stream for reading '") + filename + "'.");


    stream.read((char*)&magic, sizeof(magic));
    if(magic != 0x00000561)
        throw RushException(std::string("Binary language file '") + filename + "' magic number is not correct.");

    std::string last;
    while(stream.peek() != EOF && !stream.eof())
    {
        label.emplace_back(ReadString(stream, (estream.is_open()? &estream : nullptr)), 0);
    }

    return *this;
}


RushLocalFrontendManager& RushLocalFrontendManager::ReadFromSource(const std::string& filename)
{
    RushParser parser(filename.c_str());
    
    auto& labels = parser.GetLabels();
    if(labels.size())
    {
        if(labels.size() > 1)
            throw RushException(std::string("More than one label in the frontend source file '") + filename + "'.");

        this->label = std::move(labels.front().second);
    }

    return *this;
}

RushLocalFrontendManager& RushLocalFrontendManager::WriteToBinary(const std::string& filename)
{
    const uint32_t magic = 0x00000561;
    std::ofstream stream(filename, std::ios::trunc | std::ios::binary);
    std::ofstream estream(filename + ".EXTRA", std::ios::trunc | std::ios::binary);

    if(!stream.is_open() || !estream.is_open())
        throw RushException(std::string("Failed to open binary files for writing '") + filename + "'.");

    stream.exceptions(std::ios::failbit | std::ios::badbit);
    estream.exceptions(std::ios::failbit | std::ios::badbit);

    stream.write((char*)&magic, sizeof(magic));
    for(auto& sub : label)
    {
        stream.write(sub.first.data(), sub.first.length());
        stream.put(0);
    }

    estream.seekp(std::streamoff(stream.tellp()) - 1);
    estream.put(0);

    return *this;
}

RushLocalFrontendManager& RushLocalFrontendManager::WriteToSource(const std::string& filename)
{
    std::ofstream stream(filename, std::ios::trunc);
    stream.exceptions(std::ios::failbit | std::ios::badbit);

    stream << "[MAIN]\n";
    for(auto& sub : label)
        stream << "\"" << EscapeString(sub.first) << "\"\n";

    return *this;
}
