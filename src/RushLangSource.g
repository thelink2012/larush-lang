/*
 *	Rush Localization File Source Grammar
 *
 *	Basic structure:
 *		[label]
 *		"index 0 text"
 *		"index 1 text"@300   { @300 means at 300 miliseconds of cutscene show this text }
 *				     { Ah yeah, this is
 *		                       a comment }
 *		[other_label]
 *		{ And so on }
 *
 */
grammar RushLangSource;

/* ANTLR Options */
options {
  language=C;		// Output the lexer/parser in C++
  output=AST;		// The lexer/parser should output an Abstract Syntax Tree
}

/* Imaginary tokens */
tokens {
  CONTENT;
  SUBTITLE;		// Subtitle information down the AST tree
}


/* ------------------------  Parser Rules ------------------------  */

// Content of the source file
content			:	EOL* entry*							-> ^(CONTENT entry*);

// How to declare a label?
label			:	'[' Identifier ']' 					-> Identifier;

// A label with it's respective subtitles
entry			:	label EOL+ subtitleList 			-> ^(label subtitleList);

// List of N subtitles separated by new lines
subtitleList	:	(subtitle ((WSS* EOL+)|WSS* EOF))*	-> subtitle*;

// How to declare a subtitle? It can be a timed subtitle or frontend subtitle
subtitle		:	(subtitleWithTime|subtitleDefault);

// How to declare a default subtitle?
subtitleDefault :	String								-> ^(SUBTITLE String);

// How to declare a timed subtitle?
subtitleWithTime:	String '@' Integer					-> ^(SUBTITLE String Integer);


/* ------------------------ Token Rules ------------------------  */

// A comment is described with { this token }
Comment	:	'{' (~'}')* '}' {$channel=HIDDEN;} ;

// End of line description
EOL 	:	(('\r\n'|'\n'))+;

// Single whitespace
WSS		:	(' '|'\t'|'\v');

// At least one whitespace
WS  	:	 WSS+;

// A identifier cannot start with a digit, but then it can have digits
Identifier	: (IdentifierChar)(IdentifierChar|Digit|' ')* ;     // We support ' ' because of a leftover

// The fragment of a identifier (chars allowed, except digits)
fragment
IdentifierChar: ('A'..'Z'|'a'..'z'|'_');

// A string, as "in here, allows \"escape\" sequences"
String		: '"' (~('"'|'\\') | EscapeSequence)* '"' ;

// A integer, a bunch of digits
Integer 	:	Digit+;

// The fragment of a default digit
fragment
Digit 		:	 '0'..'9';

// The fragment of a hex digit
fragment
XDigit : (Digit|'a'..'f'|'A'..'F') ;

// The fragment of a string escape sequence
fragment
EscapeSequence
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    //|   OctalEscape
    //|   UnicodeEscape
    ;

// The fragment of an octal escape sequence
fragment
OctalEscape
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

// The fragment of an unicode escape sequence
fragment
UnicodeEscape
    :   '\\' 'u' XDigit XDigit XDigit XDigit
    ;


