#include <iostream>
#include <tclap/CmdLine.h>
#include "rush.hpp"

namespace tclap = TCLAP;

//
// To compile language files into binaries:
// larush-lang --subtitles ./ English.txt French.txt German.txt Italian.txt Spanish.txt                 # outputs files as in labels at "./"
// larush-lang --apptext   AppText.dat English.txt French.txt German.txt Italian.txt Spanish.txt        # outputs AppText.dat
// larush-lang --frontend  Local_E_V1_xbox.dat Local_E_V1_xbox.txt                                      # outputs Local_E_V1_xbox.dat
//
// To decompile the language binaries add the '-d' or '--decompile'
// larush-lang -d --subtitles Subtitles/ English.txt French.txt German.txt Italian.txt Spanish.txt      # decompiles all *.dat files from folder into the lang files
// larush-lang -d --apptext   AppText.dat English.txt French.txt German.txt Italian.txt Spanish.txt     # decompiles AppText.dat into lang files
// larush-lang -d --frontend  Local_E_V1_xbox.dat                                                       # decompiles Local_E_V1_xbox.dat into .txt
//
//

int main(int argc, char* argv[])
{
    try
    {
        tclap::CmdLine cmd("_TEST_");

        tclap::UnlabeledMultiArg<std::string> inputArg("inout", "Input and output files to compile/decompile", true, "files", cmd);
        tclap::SwitchArg decompileOpt("d", "decompile", "Performs decompilation of language binaries instead of compilation", cmd);
        tclap::SwitchArg subtitlesOpt("", "subtitles", "The type of operation is for subtitle files");
        tclap::SwitchArg appTextOpt("", "apptext", "The type of operation is for AppText files");
        tclap::SwitchArg frontendOpt("", "frontend", "The type of operation is for local frontend files");
        cmd.xorAdd( std::vector<tclap::Arg*> { &subtitlesOpt, &appTextOpt, &frontendOpt } );

        // TODO help text

        cmd.parse(argc, argv);
        auto decompile = decompileOpt.isSet();

        if(subtitlesOpt.isSet())
        {
            RushSubtitleManager subtitles;
            auto inout_size = inputArg.getValue().size();

            if(inout_size < 2) throw RushException("Missing output file argument(s).");

            auto& arg1   = inputArg.getValue()[0];
            auto  argx = std::vector<std::string>(inputArg.getValue().begin() + 1, inputArg.getValue().end());

            if(decompile)
                subtitles.ReadFromBinary(arg1).WriteToSource(argx);
            else
                subtitles.ReadFromSource(argx).WriteToBinary(arg1);
        }
        else if(frontendOpt.isSet())
        {
            RushLocalFrontendManager frontend;

            std::string dat, txt;
            auto& inout = inputArg.getValue();
            
            if(inout.empty())
                throw RushException("Missing input/output dat file argument.");

            dat = inout[0];
            txt = inout.size() >= 2? inout[1] : RushLangManager::RemoveExtension(dat) + ".txt";              

            if(decompile)
                frontend.ReadFromBinary(dat).WriteToSource(txt);
            else
                frontend.ReadFromSource(txt).WriteToBinary(dat);
        }
        else if(appTextOpt.isSet())
        {
        }

    }
    catch(const tclap::ArgException& e)
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        return EXIT_FAILURE;
    }
    catch(const std::exception& e)
    {
        std::cerr << "error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
